output "project" {
  value = "${var.argocd_project_name}-${var.project_environment}"
}