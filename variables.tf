variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}

variable "argocd_namespace" {
  type    = string
  default = "argocd"
}
variable "argocd_project_name" {
  type = string
}
variable "argocd_project_description" {
  type = string
}
variable "argocd_cluster_name" {
  type    = string
  default = "in-cluster"
}
variable "argocd_cluster_url" {
  type    = string
  default = "https://kubernetes.default.svc"
}
