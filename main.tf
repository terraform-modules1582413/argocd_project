resource "kubernetes_manifest" "argocd_project" {
  manifest = yamldecode(
    templatefile("${path.module}/argocd-project.yaml", {
      project_name        = "${var.project_prefix}-${var.argocd_project_name}-${var.project_environment}"
      project_description = var.argocd_project_description
      argocd_namespace    = var.argocd_namespace
      cluster_name        = var.argocd_cluster_name
      cluster_url         = var.argocd_cluster_url
    })
  )
}
