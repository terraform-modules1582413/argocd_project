<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.18.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.18.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_manifest.argocd_project](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_argocd_cluster_name"></a> [argocd\_cluster\_name](#input\_argocd\_cluster\_name) | n/a | `string` | `"in-cluster"` | no |
| <a name="input_argocd_cluster_url"></a> [argocd\_cluster\_url](#input\_argocd\_cluster\_url) | n/a | `string` | `"https://kubernetes.default.svc"` | no |
| <a name="input_argocd_namespace"></a> [argocd\_namespace](#input\_argocd\_namespace) | n/a | `string` | `"argocd"` | no |
| <a name="input_argocd_project_description"></a> [argocd\_project\_description](#input\_argocd\_project\_description) | n/a | `string` | n/a | yes |
| <a name="input_argocd_project_name"></a> [argocd\_project\_name](#input\_argocd\_project\_name) | n/a | `string` | n/a | yes |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_project"></a> [project](#output\_project) | n/a |
<!-- END_TF_DOCS -->